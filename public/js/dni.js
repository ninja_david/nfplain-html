/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_script_sourcer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/script-sourcer */ "./src/modules/script-sourcer.js");
/* harmony import */ var _modules_url_params__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/url-params */ "./src/modules/url-params.js");
/* harmony import */ var _modules_cookie_manager__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/cookie-manager */ "./src/modules/cookie-manager.js");
/* harmony import */ var _modules_analytics__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/analytics */ "./src/modules/analytics.js");
/* harmony import */ var _modules_utility__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/utility */ "./src/modules/utility.js");
/* harmony import */ var _modules_dni_url_generator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modules/dni-url-generator */ "./src/modules/dni-url-generator.js");
/* harmony import */ var _modules_number_restorer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modules/number-restorer */ "./src/modules/number-restorer.js");
/* harmony import */ var _modules_number_map__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modules/number-map */ "./src/modules/number-map.js");











(function () {
  var doneSent = false;
  var ntHermesCookieName = 'nt_hermes';
  var ntDoneEventName = 'nt_done';
  var ntHermesCookie = _modules_cookie_manager__WEBPACK_IMPORTED_MODULE_2__["default"].get(ntHermesCookieName); // register the replacement

  if (window.jQuery && !!window.jQuery(window).on) {
    window.jQuery(window).on("load", replaceNumbers());
  } else if (window.addEventListener) {
    window.addEventListener('load', replaceNumbers, false);
  } else if (window.attachEvent) {
    window.attachEvent('onload', replaceNumbers);
  } else {
    var oldonload = window.onload;

    if (typeof window.onload != 'function') {
      window.onload = replaceNumbers;
    } else {
      window.onload = function () {
        if (oldonload) {
          oldonload();
        }

        replaceNumbers();
      };
    }
  }

  if (window.jQuery && !!window.jQuery(document).on) {
    //jquery mobile < 1.4
    window.jQuery(document).on('pagebeforeshow', function (event, data) {
      replaceNumbers();
    }); //jquery mobile > 1.4

    window.jQuery(document).on('pagecontainerbeforetransition', function (event, data) {
      replaceNumbers();
    });
  }

  document.addEventListener(ntDoneEventName, function () {
    if (_modules_number_map__WEBPACK_IMPORTED_MODULE_7__["default"].getItemsCount()) {
      _modules_number_restorer__WEBPACK_IMPORTED_MODULE_6__["default"].restore(_modules_number_map__WEBPACK_IMPORTED_MODULE_7__["default"]);
    }
  });

  function replaceNumbers() {
    var UA = navigator.userAgent;

    var blockedUA = function () {
      var blockUAs = ['Googlebot', 'Web Preview'];

      if (typeof document.webkitVisibilityState != 'undefined') {
        if (document.webkitVisibilityState == 'prerender') {
          return true;
        }
      }

      for (var i = 0; i < blockUAs.length; i++) {
        if (UA.match(blockUAs[i])) {
          logError(blockUAs[i]);
          return true;
        }
      }

      return false;
    }();

    if (blockedUA) {
      return false;
    }

    if (ntHermesCookie && _modules_cookie_manager__WEBPACK_IMPORTED_MODULE_2__["default"].parse(ntHermesCookie)) {
      _sendDoneEvent();
    } else {
      _getNumberReplacementData();
    }
  }

  function _getNumberReplacementData() {
    var defaultDniHostname = 'calls.mymarketingreports.com';
    var scriptSource = _modules_script_sourcer__WEBPACK_IMPORTED_MODULE_0__["default"].getSrc(/js\/dni\.js/);
    var dniHostname = _modules_url_params__WEBPACK_IMPORTED_MODULE_1__["default"].getVar('dnihost', scriptSource) || defaultDniHostname;

    var id = function () {
      var id = _modules_url_params__WEBPACK_IMPORTED_MODULE_1__["default"].getVar('nt_id', scriptSource) || _modules_url_params__WEBPACK_IMPORTED_MODULE_1__["default"].getVar('ct_id', window.location.href);

      if (typeof tracking_ids != 'undefined') {
        id = tracking_ids.toString();
      }

      if (typeof ninjatrack_ids != 'undefined') {
        id = ninjatrack_ids.toString();
      }

      return id;
    }();

    var url = Object(_modules_dni_url_generator__WEBPACK_IMPORTED_MODULE_5__["default"])(dniHostname, id, Object(_modules_analytics__WEBPACK_IMPORTED_MODULE_3__["default"])(_modules_utility__WEBPACK_IMPORTED_MODULE_4__["default"]), _modules_cookie_manager__WEBPACK_IMPORTED_MODULE_2__["default"].analyticsID);
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);

    xhr.onload = function () {
      if (this.status === 200) {
        _handleNumberResponse(JSON.parse(this.responseText));
      }
    };

    xhr.send();
  }

  function _handleNumberResponse(data) {
    for (var assignedNumber in data) {
      if (data[assignedNumber].hasOwnProperty('data')) {
        var replacementData = data[assignedNumber].data;
        var originalNumber = replacementData.number.original;
        var trackingNumber = replacementData.number.tracking;
        var formattedNumber = replacementData.number.formatted;
        var country = replacementData.country;
        var analyticsNid = replacementData.analyticsNid;
        var checkServerForReplace = replacementData.checkServerForReplace ? 1 : 0;
        var expirationTime = replacementData.expirationTime;
        var content = "".concat(originalNumber, "|").concat(trackingNumber, "|").concat(formattedNumber, "|").concat(country, "|").concat(analyticsNid, "|").concat(checkServerForReplace, "|").concat(expirationTime);
        _modules_number_map__WEBPACK_IMPORTED_MODULE_7__["default"].addNumber(formattedNumber, trackingNumber, country, replacementData.ttl);
        _modules_cookie_manager__WEBPACK_IMPORTED_MODULE_2__["default"].set(ntHermesCookieName, content, replacementData.ttl, replacementData.counter);
      }
    }

    ntHermesCookie = _modules_cookie_manager__WEBPACK_IMPORTED_MODULE_2__["default"].get(ntHermesCookieName);

    if (ntHermesCookie) {
      _modules_cookie_manager__WEBPACK_IMPORTED_MODULE_2__["default"].parse(ntHermesCookie);
    }

    _sendDoneEvent();
  }

  function _sendDoneEvent() {
    if (!doneSent) {
      var ntDoneEvent = document.createEvent('Event');
      ntDoneEvent.initEvent(ntDoneEventName, true, true);
      document.dispatchEvent(ntDoneEvent);
      doneSent = true;
    }
  }
})();

/***/ }),

/***/ "./src/modules/analytics.js":
/*!**********************************!*\
  !*** ./src/modules/analytics.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cookie_manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./cookie-manager */ "./src/modules/cookie-manager.js");


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }



function _parseUri(str) {
  var o = {
    strictMode: true,
    key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
    q: {
      name: "queryKey",
      parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
      strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
      loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
  };
  var m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
      uri = {},
      i = 14;

  while (i--) {
    uri[o.key[i]] = m[i] || "";
  }

  uri[o.q.name] = {};
  uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
    if ($1) uri[o.q.name][$1] = $2;
  });
  return uri;
}

function _getReferrer(doc) {
  return doc.referrer !== undefined ? doc.referrer : doc.location.href;
}

function _getQuery(doc) {
  var query = doc.location.search;
  var pcsId = _cookie_manager__WEBPACK_IMPORTED_MODULE_0__["default"].get('_pcsid', null);

  if (pcsId) {
    if (query && query !== '') {
      query += "&pc_source_id=".concat(pcsId);
    }
  }

  var pcuId = _cookie_manager__WEBPACK_IMPORTED_MODULE_0__["default"].get('_pcUID', null);

  if (pcuId) {
    if (query && query !== '') {
      query += "&pc_user_id=".concat(pcuId);
    }
  }

  return query;
}

/* harmony default export */ __webpack_exports__["default"] = (function (util) {
  if (typeof util === 'undefined' || _typeof(util) !== 'object' || util === null) {
    throw new Error('Analytics needs a utility object');
  }

  if (!util.hasOwnProperty('getWindowObject') || !util.hasOwnProperty('getDocumentObject')) {
    throw new Error("Analytics needs a utility object with 'getWindowObject' and 'getDocumentObject' properties");
  }

  var window = util.getWindowObject();
  var document = util.getDocumentObject();
  return {
    query: _getQuery(document),
    referrer: _parseUri(_getReferrer(document)).source,
    hostname: window.location.hostname,
    pathname: window.location.pathname,
    cid: function () {
      if (window.ga) {
        ga(function (tracker) {
          return tracker.get('clientId');
        });
      }

      return null;
    }()
  };
});
;

/***/ }),

/***/ "./src/modules/cookie-manager.js":
/*!***************************************!*\
  !*** ./src/modules/cookie-manager.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _number_replacer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./number-replacer */ "./src/modules/number-replacer.js");
/* harmony import */ var _utility__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utility */ "./src/modules/utility.js");




var document = _utility__WEBPACK_IMPORTED_MODULE_1__["default"].getDocumentObject();
var analyticsID = null;

function setCookie(n, v, e, c) {
  if (c === 1) {
    var i = 0,
        domain = document.domain,
        p = domain.split('.');
    var escV = escape(v);

    if (p.length > 1) {
      while (i < p.length - 1 && document.cookie.indexOf(n + '=' + escV) == -1) {
        domain = p.slice(-1 - ++i).join('.');

        _setCookieInternal(n, escV, e, domain);
      }
    } else {
      _setCookieInternal(n, escV, e, null);
    }
  } else {
    var cur = getCookie(n, '');
    cur = cur + "{" + v;
    setCookie(n, cur, e, 1);
  }
}

function _setCookieInternal(n, v, e, d) {
  var cookie = n + "=" + v + ";path=/";

  if (d) {
    cookie += ";domain=" + d;
  }

  if (e) {
    var tempDate = new Date();
    tempDate.setDate(tempDate.getDate() + e);
    cookie += ";expires=" + tempDate.toUTCString();
  }

  document.cookie = cookie;
}

function getCookie(name, default_value) {
  var get_cookie_ret_one;

  if (document.cookie) {
    var cookie_index = document.cookie.indexOf(name);

    if (cookie_index !== -1) {
      var namestart = document.cookie.indexOf("=", cookie_index) + 1;
      var nameend = document.cookie.indexOf(";", cookie_index);

      if (nameend === -1) {
        nameend = document.cookie.length;
      }

      get_cookie_ret_one = document.cookie.substring(namestart, nameend);
    } else {
      get_cookie_ret_one = default_value;
    }
  } else {
    get_cookie_ret_one = default_value;
  }

  return get_cookie_ret_one ? unescape(get_cookie_ret_one) : default_value;
}

function parseCookie(cookie) {
  var cookies = cookie.split("{");
  var resp = -1;

  for (var index = 0; index < cookies.length; ++index) {
    var tempresp = _parseSingleCookie(cookies[index]); //we only want true if all responses are true.


    if (resp === -1 || tempresp === false) {
      resp = tempresp;
    }
  }

  if (resp === -1) return false;
  return resp;
}

function _parseSingleCookie(cookie) {
  var parts = cookie.split("|");
  var replaceNow = true;

  if (parts.length > 6) {
    //if the keyword number is still good - just use it.
    replaceNow = parts[6] > new Date().getTime() / 1000;
  }

  if (replaceNow) {
    _number_replacer__WEBPACK_IMPORTED_MODULE_0__["default"].replace(parts[0], parts[1], parts[2], parts[3]);
    return true;
  } else {
    analyticsID = parts[4];
    return false;
  }
}

/* harmony default export */ __webpack_exports__["default"] = ({
  get: getCookie,
  set: setCookie,
  parse: parseCookie,
  analyticsID: analyticsID
});

/***/ }),

/***/ "./src/modules/dni-url-generator.js":
/*!******************************************!*\
  !*** ./src/modules/dni-url-generator.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* harmony default export */ __webpack_exports__["default"] = (function (dniHostname, accountId, analytics, analyticsID) {
  if (!dniHostname) {
    throw new Error('Dni url generation requires a hostname');
  }

  if (!accountId) {
    throw new Error('Dni url generation requires an account id');
  }

  if (typeof analytics === 'undefined' || _typeof(analytics) !== 'object' || analytics === null) {
    throw new Error('Analytics must be an object for dni url generation');
  }

  var url = "https://".concat(dniHostname, "/dni?id=").concat(accountId);

  for (var key in analytics) {
    if (analytics.hasOwnProperty(key) && analytics[key]) {
      switch (key) {
        case 'query':
          url += "&q=".concat(encodeURIComponent(analytics.query));
          break;

        case 'referrer':
          url += "&src=".concat(encodeURIComponent(unescape(analytics.referrer)));
          break;

        case 'hostname':
          url += "&h=".concat(analytics.hostname);
          break;

        case 'pathname':
          url += "&p=".concat(analytics.pathname);
          break;

        case 'cid':
          url += "&cid=".concat(analytics.cid);
          break;
      }
    }
  }

  if (analyticsID) {
    url += "&aid=".concat(analyticsID);
  }

  return url;
});

/***/ }),

/***/ "./src/modules/number-map.js":
/*!***********************************!*\
  !*** ./src/modules/number-map.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);


var mapKey = 'nt-number-map';
var version = 1;

function _getMap() {
  var jsonMap = localStorage.getItem(mapKey);

  if (!jsonMap) {
    var newMap = {
      v: version,
      items: {}
    };
    localStorage.setItem(mapKey, JSON.stringify(newMap));
    jsonMap = localStorage.getItem(mapKey);
  }

  return _cleanMap(JSON.parse(jsonMap));
}

function _cleanMap(map) {
  var now = new Date();

  for (var id in map.items) {
    var expiresAt = Date.parse(map.items[id].expiresAt);

    if (expiresAt <= now) {
      delete map.items[id];
    }
  }

  if (_saveMap(map)) {
    return map;
  }

  return null;
}

function _saveMap(map) {
  localStorage.setItem(mapKey, JSON.stringify(map));
  return true;
}

function getItems() {
  return _getMap().items;
}

function getItemsCount() {
  return Object.keys(getItems()).length;
}

function clearItems() {
  var map = _getMap();

  map.items = {};

  _saveMap(map);

  return getItems();
}

function addNumber(textNumber, number, country, ttl) {
  var map = _getMap();

  var id = _generateUniqueId(10);

  var expirationDate = new Date();
  expirationDate.setDate(expirationDate.getDate() + (ttl || 30));
  map.items[id] = {
    id: id,
    href: number.replace('+', ''),
    text: textNumber,
    country: country,
    expiresAt: expirationDate.toUTCString()
  };

  _saveMap(map);

  return id;
}

function updateNumber(numId, textNumber, number) {
  var map = _getMap();

  map.items[numId].href = number;
  map.items[numId].text = textNumber;

  _saveMap(map);

  return true;
}

function hasNumber(number) {
  return !!getIdForNumber(number);
}

function getIdForNumber(number) {
  var items = getItems();
  return Object.keys(items).find(function (key) {
    return items[key].href === number;
  });
}

function hasFormattedNumber(textNumber) {
  return !!getIdForFormattedNumber(textNumber);
}

function getIdForFormattedNumber(textNumber) {
  var items = getItems();
  return Object.keys(items).find(function (key) {
    return items[key].text === textNumber;
  });
}

function getRandomId() {
  var keys = Object.keys(getItems());
  return keys.length ? keys[Math.floor(Math.random() * keys.length)] : null;
}

function getNumberData(numberId) {
  return getItems()[numberId];
}

function _generateUniqueId(length) {
  var items = getItems();
  var id = null;

  while (!id) {
    var tempId = _generateId(length);

    if (!items.hasOwnProperty(tempId)) {
      id = tempId;
    }
  }

  return id;
}

function _generateId(length) {
  var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charsLen = chars.length;
  var result = '';

  for (var i = 0; i < length; i++) {
    result += chars.charAt(Math.floor(Math.random() * charsLen));
  }

  return result;
}

/* harmony default export */ __webpack_exports__["default"] = ({
  getItems: getItems,
  getItemsCount: getItemsCount,
  clearItems: clearItems,
  hasNumber: hasNumber,
  hasFormattedNumber: hasFormattedNumber,
  addNumber: addNumber,
  updateNumber: updateNumber,
  getNumberData: getNumberData,
  getIdForNumber: getIdForNumber,
  getIdForFormattedNumber: getIdForFormattedNumber,
  getRandomId: getRandomId
});

/***/ }),

/***/ "./src/modules/number-replacer.js":
/*!****************************************!*\
  !*** ./src/modules/number-replacer.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utility */ "./src/modules/utility.js");
/* harmony import */ var _number_map__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./number-map */ "./src/modules/number-map.js");





function replaceSingleRegexSwitch(numToFind, numToInsert, formattedNumber, country) {
  switch (country) {
    case 'te':
      _replaceSingleRegexTest(numToFind, numToInsert, formattedNumber);

      break;

    case 'GB':
      _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, '44');

      break;

    case 'IE':
      _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, '353');

      break;

    case 'AU':
      _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, '61');

      break;

    case 'NZ':
      _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, '64');

      break;

    case 'FR':
      _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, '33');

      break;

    case 'DE':
      _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, '49');

      break;

    case 'IT':
      _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, '39');

      break;

    case 'CL':
      _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, '56');

      break;

    case 'CH':
      _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, '41');

      break;

    case 'HK':
      _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, '852');

      break;

    case 'AR':
      _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, '54');

      break;

    default:
      _replaceSingleRegex(numToFind, numToInsert, formattedNumber);

      break;
  }
}

function _buildSingleNumberRegex(numToFindS) {
  var fullExpressionS = numToFindS.replace(/[.?*+^$[\]\\/(){}|-]/g, "\\$&");
  var iterationS = new Array();
  numToFindS = numToFindS.replace(/[\s.?*+^$[\]\\/(){}|-]/g, "");
  var countryCode, areaCode, exchange, remain;

  if (numToFindS.substring(0, 1) == '1') {
    countryCode = '';
    areaCode = numToFindS.substring(1, 4);
    exchange = numToFindS.substring(4, 7).replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    remain = numToFindS.substring(7).replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
  } else {
    countryCode = '';
    areaCode = numToFindS.substring(0, 3);
    exchange = numToFindS.substring(3, 6).replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    remain = numToFindS.substring(6).replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
  }

  iterationS[0] = countryCode + "\\([\\s\\u00A0\\u00AD]*" + areaCode + "[\\s\\u00A0\\u00AD]*\\)[\\-\\.\\s\\u00A0\\u00AD]*" + exchange + "[\\-\\.\\s\\u00A0\\u00AD]*" + remain + "(?!\\d)(?=[<\\s\\u00A0\\u00AD\\.\\!\\?\\,]?)";
  iterationS[1] = countryCode + areaCode + "[\\-\\.\\s\\u00A0\\u00AD]*" + exchange + "[\\-\\.\\s\\u00A0\\u00AD]*" + remain + "(?!\\d)(?=[<\\s\\u00A0\\u00AD\\.\\!\\?\\,]?)";
  iterationS[2] = countryCode + "(" + areaCode + ")[\\u00A0\\u00AD\\s]*(" + exchange + ")[\xA0\xAD\\s]*(" + remain + ")(?!\\d)(?=[<\\s\\u00A0\\u00AD\\.\\!\\?\\,]?)";
  var iCountS = iterationS.length - 1;

  for (var i = 0; i <= iCountS; i++) {
    fullExpressionS = fullExpressionS + "|" + iterationS[i];
  }

  return new RegExp(fullExpressionS, "gi");
}

function _buildSingleNumberRegexUK(numToFindS, code) {
  var fullExpressionS = numToFindS.replace(/[.?*+^$[\]\\/(){}|-]/g, "\\$&");
  var iterationS = [];
  var start = 0;
  numToFindS = numToFindS.replace(/[\s.?*+^$[\]\\/(){}|-\u00A0|-\u00AD]/g, "");

  if (numToFindS.substring(0, code.length) == code) {
    start = code.length;
  }

  if (numToFindS.substring(0, code.length + 1) == '+' + code) {
    start = code.length + 1;
  }

  var stringFormat = "\\([\\u00A0\\u00AD\\s]*";
  var stringFormat2 = " ?\\)?[\\-\\.\\u00A0\\u00AD\\s]*";
  var stringFormat3 = "[\\-\\.\\u00A0\\u00AD\\s]*";
  var stringFormat4 = "(?!\\d)";
  var areaCode1 = numToFindS.substring(start, start + 1);
  var areaCode2 = numToFindS.substring(start, start + 2);
  var areaCode3 = numToFindS.substring(start, start + 3);
  var areaCode4 = numToFindS.substring(start, start + 4);
  var exchange1_3 = numToFindS.substring(start + 1, start + 4);
  var exchange2_4 = numToFindS.substring(start + 2, start + 6);
  var exchange2_3 = numToFindS.substring(start + 2, start + 5);
  var exchange3_3 = numToFindS.substring(start + 3, start + 6);
  var exchange4_6 = numToFindS.substring(start + 4, start + 10);
  var exchange3_2 = numToFindS.substring(start + 3, start + 5);
  var exchange3_6 = numToFindS.substring(start + 3, start + 9);
  var exchange4_5 = numToFindS.substring(start + 4, start + 9);
  var exchange4_3 = numToFindS.substring(start + 4, start + 7);
  var remain3 = numToFindS.substring(start + 3);
  var remain4 = numToFindS.substring(start + 4);
  var remain5 = numToFindS.substring(start + 5);
  var remain6 = numToFindS.substring(start + 6);
  var remain7 = numToFindS.substring(start + 7);

  if (numToFindS.length == start + 8) {
    //1-3-4
    fullExpressionS += "|" + stringFormat + areaCode1 + stringFormat2 + exchange1_3 + stringFormat3 + remain4 + stringFormat4;
    fullExpressionS += "|" + areaCode1 + stringFormat2 + exchange1_3 + stringFormat3 + remain4 + stringFormat4; //2-3-3

    fullExpressionS += "|" + stringFormat + areaCode2 + stringFormat2 + exchange2_3 + stringFormat3 + remain5 + stringFormat4;
    fullExpressionS += "|" + areaCode2 + stringFormat2 + exchange2_3 + stringFormat3 + remain5 + stringFormat4; //3-5

    fullExpressionS += "|" + stringFormat + areaCode3 + stringFormat2 + remain3 + stringFormat4;
    fullExpressionS += "|" + areaCode3 + stringFormat2 + remain3 + stringFormat4; //4-4

    fullExpressionS += "|" + stringFormat + areaCode4 + stringFormat2 + remain4 + stringFormat4;
    fullExpressionS += "|" + areaCode4 + stringFormat2 + remain4 + stringFormat4;
  } else if (numToFindS.length == start + 9) {
    //1-2-2-2-2
    fullExpressionS += "|" + stringFormat + areaCode1 + stringFormat2 + numToFindS.substring(start + 1, start + 3) + stringFormat3 + numToFindS.substring(start + 3, start + 5) + stringFormat3 + numToFindS.substring(start + 5, start + 7) + stringFormat3 + numToFindS.substring(start + 7) + stringFormat4;
    fullExpressionS += "|" + areaCode1 + stringFormat2 + numToFindS.substring(start + 1, start + 3) + stringFormat3 + numToFindS.substring(start + 3, start + 5) + stringFormat3 + numToFindS.substring(start + 5, start + 7) + stringFormat3 + numToFindS.substring(start + 7) + stringFormat4; //2-3-2-2

    fullExpressionS += "|" + stringFormat + areaCode2 + stringFormat2 + numToFindS.substring(start + 2, start + 5) + stringFormat3 + numToFindS.substring(start + 5, start + 7) + stringFormat3 + numToFindS.substring(start + 7) + stringFormat4;
    fullExpressionS += "|" + areaCode2 + stringFormat2 + numToFindS.substring(start + 2, start + 5) + stringFormat3 + numToFindS.substring(start + 5, start + 7) + stringFormat3 + numToFindS.substring(start + 7) + stringFormat4; //2-3-4

    fullExpressionS += "|" + stringFormat + areaCode2 + stringFormat2 + exchange2_3 + stringFormat3 + remain5 + stringFormat4;
    fullExpressionS += "|" + areaCode2 + stringFormat2 + exchange2_3 + stringFormat3 + remain5 + stringFormat4; //3-2-4

    fullExpressionS += "|" + stringFormat + areaCode3 + stringFormat2 + exchange3_2 + stringFormat3 + remain5 + stringFormat4;
    fullExpressionS += "|" + areaCode3 + stringFormat2 + exchange3_2 + stringFormat3 + remain5 + stringFormat4; //3-3-3

    fullExpressionS += "|" + stringFormat + areaCode3 + stringFormat2 + exchange3_3 + stringFormat3 + remain6 + stringFormat4;
    fullExpressionS += "|" + areaCode3 + stringFormat2 + exchange3_3 + stringFormat3 + remain6 + stringFormat4; //3-6

    fullExpressionS += "|" + stringFormat + areaCode3 + stringFormat2 + remain3 + stringFormat4;
    fullExpressionS += "|" + areaCode3 + stringFormat2 + remain3 + stringFormat4; //4-5

    fullExpressionS += "|" + stringFormat + areaCode4 + stringFormat2 + remain4 + stringFormat4;
    fullExpressionS += "|" + areaCode4 + stringFormat2 + remain4 + stringFormat4;
  } //10 digit numbers
  else {
      //2-2-2-2-2
      fullExpressionS += "|" + stringFormat + areaCode2 + stringFormat2 + numToFindS.substring(start + 2, start + 4) + stringFormat3 + numToFindS.substring(start + 4, start + 6) + stringFormat3 + numToFindS.substring(start + 6, start + 8) + stringFormat3 + numToFindS.substring(start + 8) + stringFormat4;
      fullExpressionS += "|" + areaCode2 + stringFormat2 + numToFindS.substring(start + 2, start + 4) + stringFormat3 + numToFindS.substring(start + 4, start + 6) + stringFormat3 + numToFindS.substring(start + 6, start + 8) + stringFormat3 + numToFindS.substring(start + 8) + stringFormat4; //2-4-4

      fullExpressionS += "|" + stringFormat + areaCode2 + stringFormat2 + exchange2_4 + stringFormat3 + remain6 + stringFormat4;
      fullExpressionS += "|" + areaCode2 + stringFormat2 + exchange2_4 + stringFormat3 + remain6 + stringFormat4; //2-3-5

      fullExpressionS += "|" + stringFormat + areaCode2 + stringFormat2 + exchange2_3 + stringFormat3 + remain5 + stringFormat4;
      fullExpressionS += "|" + areaCode2 + stringFormat2 + exchange2_3 + stringFormat3 + remain5 + stringFormat4; //3-3-4

      fullExpressionS += "|" + stringFormat + areaCode3 + stringFormat2 + exchange3_3 + stringFormat3 + remain6 + stringFormat4;
      fullExpressionS += "|" + areaCode3 + stringFormat2 + exchange3_3 + stringFormat3 + remain6 + stringFormat4; //4-6

      fullExpressionS += "|" + stringFormat + areaCode4 + stringFormat2 + remain4 + stringFormat4;
      fullExpressionS += "|" + areaCode4 + stringFormat2 + exchange4_6 + stringFormat4; //4-3-3

      fullExpressionS += "|" + stringFormat + areaCode4 + stringFormat2 + exchange4_3 + stringFormat3 + remain7 + stringFormat4;
      fullExpressionS += "|" + areaCode4 + stringFormat2 + exchange4_3 + stringFormat3 + remain7 + stringFormat4;
    }

  fullExpressionS += "|" + numToFindS.substring(start);
  return new RegExp(fullExpressionS, "gi");
} //this method supports string replacements.


function _replaceSingleRegexTest(numToFind, numToInsert, formattedNumber) {
  if (numToFind.length === 0) {
    return;
  }

  _domIterator(numToFind, numToInsert, formattedNumber, null);
} //only digits


function _replaceSingleRegex(numToFind, numToInsert, formattedNumber) {
  numToFind = numToFind.trim();

  if (numToFind.length === 0) {
    return;
  }

  var rregex = _buildSingleNumberRegex(numToFind);

  if (formattedNumber.substring(0, 2) === '1 ') {
    formattedNumber = formattedNumber.substring(2);
  } else if (formattedNumber.substring(0, 1) === '1') {
    formattedNumber = formattedNumber.substring(1);
  }

  if (numToInsert.substring(0, 1) === '+') {
    numToInsert = numToInsert.substring(1);
  }

  _domIterator(rregex, numToInsert, formattedNumber, null);
}

function _replaceSingleRegexAlt(numToFind, numToInsert, formattedNumber, code) {
  numToFind = numToFind.trim();

  if (numToFind.length === 0) {
    return;
  }

  var rregex = _buildSingleNumberRegexUK(numToFind, code);

  if (formattedNumber.substring(0, code.length + 1) == code + ' ') {
    formattedNumber = formattedNumber.substring(code.length + 1);
  } else if (formattedNumber.substring(0, code.length) == code) {
    formattedNumber = formattedNumber.substring(code.length);
  }

  if (numToInsert.substring(0, 1) == '+') {
    numToInsert = numToInsert.substring(1);
  }

  _domIterator(rregex, numToInsert, formattedNumber, null);
}

function _domIterator(regex, replacementNumber, formattedNumber, node) {
  var childNodes = (node || document.body).childNodes;
  var cnLength = childNodes.length;
  var excludes = ['html', 'head', 'style', 'link', 'title', 'meta', 'script', 'iframe'];
  var numId = null;

  if (!_number_map__WEBPACK_IMPORTED_MODULE_1__["default"].hasNumber(replacementNumber)) {
    numId = _number_map__WEBPACK_IMPORTED_MODULE_1__["default"].addNumber(null, replacementNumber);
  } else {
    numId = _number_map__WEBPACK_IMPORTED_MODULE_1__["default"].getIdForNumber(replacementNumber);
  }

  while (cnLength--) {
    var currentNode = childNodes[cnLength];

    if (currentNode.nodeType === Node.ELEMENT_NODE) {
      if (currentNode.nodeName === 'A' && (currentNode.href.match(/^tel:/) || currentNode.href.match(/\/tel\//))) {
        var pot = currentNode.href.replace(regex, replacementNumber);

        if (pot !== currentNode.href) {
          if (currentNode.href.match(/^tel:/)) {
            currentNode.href = "tel:+" + replacementNumber;
          } else if (currentNode.href.match(/\/tel\//)) {
            currentNode.href = currentNode.href.replace(/tel\/\+\d{7,16}/i, "tel/+" + replacementNumber);
            currentNode.href = currentNode.href.replace(/tel\/\d{7,16}/i, "tel/+" + replacementNumber);
          }

          currentNode.setAttribute(_utility__WEBPACK_IMPORTED_MODULE_0__["default"].trackingNumberAttributeName, numId);
          currentNode.classList.add(_utility__WEBPACK_IMPORTED_MODULE_0__["default"].trackingNumberElIdentifier);
        }
      }

      if (excludes.indexOf(currentNode.nodeName.toLowerCase()) === -1 && currentNode.getAttribute("ninjatrack_ignore") == null) {
        _domIterator(regex, replacementNumber, formattedNumber, currentNode);
      }
    }

    if (currentNode == null || currentNode.nodeType !== Node.TEXT_NODE || currentNode.data === undefined) {
      continue;
    } // a match was found if still here
    // assign the replaced value


    var newText = currentNode.data.replace(regex, formattedNumber); // only replace if a change was made
    // and replace only the data of the current node.

    if (newText !== currentNode.data) {
      var parentV = currentNode.parentElement != null ? currentNode.parentElement : currentNode.parentNode;

      if (parentV != null && !parentV.getAttribute('ninjatrack_orig')) {
        parentV.setAttribute('ninjatrack_orig', encodeURIComponent(currentNode.data));

        if (!_number_map__WEBPACK_IMPORTED_MODULE_1__["default"].hasFormattedNumber(newText)) {
          var numData = _number_map__WEBPACK_IMPORTED_MODULE_1__["default"].getNumberData(numId);

          if (numData.text) {
            numId = _number_map__WEBPACK_IMPORTED_MODULE_1__["default"].addNumber(newText, replacementNumber);
          } else {
            _number_map__WEBPACK_IMPORTED_MODULE_1__["default"].updateNumber(numId, newText, replacementNumber);
          }
        } else {
          numId = _number_map__WEBPACK_IMPORTED_MODULE_1__["default"].getIdForFormattedNumber(newText);
        }

        if (numId) {
          parentV.setAttribute(_utility__WEBPACK_IMPORTED_MODULE_0__["default"].trackingNumberAttributeName, numId);
          parentV.classList.add(_utility__WEBPACK_IMPORTED_MODULE_0__["default"].trackingNumberElIdentifier);
        }
      }

      currentNode.data = newText;
      parentV.setAttribute('ninjatrack_timestamp', new Date().getTime());
    }
  }
}

/* harmony default export */ __webpack_exports__["default"] = ({
  replace: replaceSingleRegexSwitch
});

/***/ }),

/***/ "./src/modules/number-restorer.js":
/*!****************************************!*\
  !*** ./src/modules/number-restorer.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utility */ "./src/modules/utility.js");


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }


var document = _utility__WEBPACK_IMPORTED_MODULE_0__["default"].getDocumentObject();
var anchorNodeName = 'A';

function restoreNumbers(map) {
  if (typeof map === 'undefined' || _typeof(map) !== 'object' || map === null) {
    throw new Error('Number restorer needs a number map object');
  }

  if (!map.hasOwnProperty('getItems') || !map.hasOwnProperty('getNumberData')) {
    throw new Error("Number restorer needs a number map object with 'getItems' and 'getNumberData' properties");
  }

  var els = document.querySelectorAll('.' + _utility__WEBPACK_IMPORTED_MODULE_0__["default"].trackingNumberElIdentifier);
  els.forEach(function (el) {
    _restoreNumber(el, map);
  });
  window.setTimeout(restoreNumbers, 1000, map);
}

function _restoreNumber(el, map) {
  var numId = null;

  if (el.hasAttribute(_utility__WEBPACK_IMPORTED_MODULE_0__["default"].trackingNumberAttributeName)) {
    numId = el.getAttribute(_utility__WEBPACK_IMPORTED_MODULE_0__["default"].trackingNumberAttributeName);
  }

  if (!numId) {
    numId = map.getRandomId();
  }

  if (numId && map.getItems().hasOwnProperty(numId)) {
    var numData = map.getNumberData(numId);

    if (el.nodeName === anchorNodeName && el.href.includes('tel')) {
      var telStartIndex = el.href.indexOf('tel');
      var currentTel = el.href.substring(telStartIndex + 'tel'.length + 1);

      if (currentTel !== "+".concat(numData.href)) {
        el.href = "".concat(el.href.substring(0, telStartIndex + 4), "+").concat(numData.href);
      }

      if (el.childNodes.length > 1 || el.childNodes[0].nodeType !== Node.TEXT_NODE) {
        return;
      }
    }

    if (el.innerHTML !== numData.text) {
      el.innerHTML = numData.text;
    }
  }
}

/* harmony default export */ __webpack_exports__["default"] = ({
  restore: restoreNumbers
});

/***/ }),

/***/ "./src/modules/script-sourcer.js":
/*!***************************************!*\
  !*** ./src/modules/script-sourcer.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utility */ "./src/modules/utility.js");



var document = _utility__WEBPACK_IMPORTED_MODULE_0__["default"].getDocumentObject();
/* harmony default export */ __webpack_exports__["default"] = ({
  getSrc: function getSrc(regex) {
    var source = '';
    var scripts = Array.prototype.slice.call(document.getElementsByTagName('script'));
    scripts.forEach(function (script) {
      var src = '';

      if (script.getAttribute.length !== undefined) {
        src = script.src;
      } else {
        src = script.getAttribute('src', -1);
      }

      if (src.match(regex)) {
        source = src;
      }
    });
    return source;
  }
});

/***/ }),

/***/ "./src/modules/url-params.js":
/*!***********************************!*\
  !*** ./src/modules/url-params.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);


/* harmony default export */ __webpack_exports__["default"] = ({
  getVar: function getVar(name, url) {
    var searchUrl = url;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS, 'i');
    var results = regex.exec(searchUrl);

    if (results == null) {
      return "";
    } else {
      return results[1];
    }
  }
});

/***/ }),

/***/ "./src/modules/utility.js":
/*!********************************!*\
  !*** ./src/modules/utility.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);


function getWindowObject() {
  return window;
}

function getDocumentObject() {
  return document;
}

/* harmony default export */ __webpack_exports__["default"] = ({
  getWindowObject: getWindowObject,
  getDocumentObject: getDocumentObject,
  trackingNumberElIdentifier: 'nt-tracking-number',
  trackingNumberAttributeName: 'nt-tracking-number-id'
});

/***/ })

/******/ });
//# sourceMappingURL=dni.js.map